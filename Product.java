public class Product {
   private String name;
   private int price;
   private double rate;

   public Product(String name, int price, double rate){
       this.name = name;
       this.price = price;
       this.rate = rate;
   }
}
