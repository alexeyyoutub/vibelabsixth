public class Main {
    Product samsung = new Product("Galaxy S21", 1100, 3.8);
    Product apple = new Product("iPhone 14", 1000, 4.4);
    Product pixel = new Product("Pixel 7", 900, 4.2);

    Category smartphones = new Category("Смартфоны", new Product[]{samsung, apple, pixel});

    Product beer = new Product("Пиво", 80, 10.0);
    Product calmar = new Product("Кальмвр", 30, 9.0);

    Category products = new Category("Продукты", new Product[]{beer, calmar});

    Basket basket = new Basket(new Product[]{apple, beer});
    User user = new User("User", "qwerty", basket);

}
